package org.bcholmes.cartographersvg;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.bcholmes.cartographersvg.huffman.Huff;
import org.bcholmes.cartographersvg.huffman.Huffman;

public class Explode {

	public static void main(String[] args) throws Exception {
		
		if (args.length == 0) {
			System.out.println("Please specify the file name you want to convert");
		} else {
			decodeFile(new File(args[0]), 0);
		}
		
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/sample.fsc"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/testsida007.FCW"), 0);
		
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Near Earth Area.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Downloads/citysmith_jic.fcw"), 0);
//		decodeFile(new File("/Users/bcholmes/Downloads/WoG Mapping Symbols.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Downloads/256vari/All 256 colors.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Downloads/Celestial_Setting/Celestial Setting.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/fortress/fortress.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Staff and Dagger Lodge.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/TotATraveller/TotATraveller.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Downloads/JdrHouses.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/vegetation1.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/swamp/SWAMP.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/CityC.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/JSCAFWEAPONS.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/JdrHouses.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/ogl_symbolsets/OGL_ss1/InnsTaverns.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/ogl_symbolsets/OGL_ss1/OGL Buildings1 Brown.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/ogl_symbolsets/OGL_ss1/OGL Building1 Slate.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/ogl_symbolsets/OGL_ss1/OGL SS1 Parts.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/ogl_symbolsets/OGL_ss3/OGL SS3 Parts.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/greenheight/TownOf Greenheight.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Orbis Nova - Tabula Geographiae.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/village1.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Haanex and environs EN.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Elemental Underground.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Color Palette.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/ProFantasy/CC3/Examples/Khalil.FCW"), 0);
//		decodeFile(new File("/Users/administrator/Documents/VeryBasic.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/achdar/achdar.FCW"), 0);
		decodeFile(new File("/Users/bcholmes/Documents/CC3/Lady's Best Friend.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/FreeSymbols/Parchment/KT Parchment.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/FreeSymbols/Coins_Final_Version_2.1/Coins Final Version 2.1.FSC"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Laxton Abbey.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Redvale/Redvale - Arrhean Sidhe City.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/Relic Keep.fcw"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/bluemars.FCW"), 0);
//		decodeFile(new File("/Users/bcholmes/Documents/CC3/TILE END CAPS.CCX"), 0);
	}
	
	static void decodeFile(File file, int dump) throws IOException {
		byte[] b = toByteArray(file);
		FileId header = new FileId(b);
		if (header.getVersionNumber() == 24) {
			if (header.isCompressed()) {
				System.out.println("Uncompressing the input file.");
				
				b = ArrayUtils.subarray(b, 128, b.length);
				Huff huff = new Huffman().HuffDecodeBuffer(b);
				
				byte[] decoded = ArrayUtils.subarray(huff.out, 0, huff.outOffset);
				Fcw.decodeBuffer(file, header, decoded);
			} else {
				b = ArrayUtils.subarray(b, 128, b.length);
				Fcw.decodeBuffer(file, header, b);
			}
		} else {
			System.out.println("Unsupported version number : " + header.getVersionNumber());
		}
	}
	
	static byte[] toByteArray(File file) throws IOException {
		InputStream input = new FileInputStream(file);
		try {
			return IOUtils.toByteArray(input);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}
}
