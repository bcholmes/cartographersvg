package org.bcholmes.packing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class CanvasTest {

	@Test
	public void shouldTrackFilledAreas() throws Exception {
		System.out.println();
		Canvas canvas = new Canvas(10, 5);
		canvas.fill(2, 1, 4, 2);
		
		assertFalse("can't fit", canvas.canFit(5, 2, 2, 1));
		assertTrue("can fit", canvas.canFit(6, 2, 2, 1));
		canvas.fill(6, 2, 2, 1);
		canvas.print();
	}

	@Test
	public void shouldTrim() throws Exception {
		System.out.println();
		Canvas canvas = new Canvas(10, 5);
		canvas.fill(2, 1, 4, 2);
		canvas.fill(6, 2, 2, 1);
		
		canvas.trim();
		canvas.print();
		assertEquals("width", 8, canvas.getWidth());
		assertEquals("height", 3, canvas.getHeight());
	}
	
	@Test
	public void shouldFindBestLocation() throws Exception {
		System.out.println();
		Canvas canvas = new Canvas(10, 5);
		Point point = canvas.assignBestLocation(5, 4);
		assertEquals("x1", 0, point.getX());
		assertEquals("y1", 0, point.getY());
		
		Point point2 = canvas.assignBestLocation(4, 2);
		assertEquals("x2", 5, point2.getX());
		assertEquals("y2", 0, point2.getY());
		
		Point point3 = canvas.assignBestLocation(3, 2);
		assertEquals("x3", 5, point3.getX());
		assertEquals("y3", 2, point3.getY());
		canvas.print();
	}

	@Test
	public void shouldFindBestLocationWithEdgeCases() throws Exception {
		System.out.println();
		Canvas canvas = new Canvas(10, 5);
		Point point = canvas.assignBestLocation(10, 4);
		assertEquals("x1", 0, point.getX());
		assertEquals("y1", 0, point.getY());
		
		Point point2 = canvas.assignBestLocation(1, 1);
		assertEquals("x2", 0, point2.getX());
		assertEquals("y2", 4, point2.getY());
		
		canvas.print();
	}
}
