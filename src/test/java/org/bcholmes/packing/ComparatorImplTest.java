package org.bcholmes.packing;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;


public class ComparatorImplTest {

	@Test
	public void shouldSortByHeight() throws Exception {
		
		List<PackItem> list = new ArrayList<PackItem>();
		list.add(new PackItemImpl(2, 4));
		list.add(new PackItemImpl(8, 10));
		list.add(new PackItemImpl(6, 4));
		list.add(new PackItemImpl(10, 14));
		list.add(new PackItemImpl(3, 5));
		list.add(new PackItemImpl(3, 6));
		list.add(new PackItemImpl(12, 4));
		list.add(new PackItemImpl(9, 7));
		list.add(new PackItemImpl(6, 9));
		
		Collections.sort(list, new ComparatorImpl());
		
		assertEquals("height largest", 14, list.get(0).getHeight());
		assertEquals("height smallest", 4, list.get(list.size()-1).getHeight());
		
	}
	
}
