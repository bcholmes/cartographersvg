package org.bcholmes.packing;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class PackerTest {

	@Test
	public void shouldFindResult() throws Exception {
		
		List<PackItem> list = new ArrayList<PackItem>();
		list.add(new PackItemImpl(2, 4));
		list.add(new PackItemImpl(8, 10));
		list.add(new PackItemImpl(6, 4));
		list.add(new PackItemImpl(10, 14));
		list.add(new PackItemImpl(3, 5));
		list.add(new PackItemImpl(3, 6));
		list.add(new PackItemImpl(12, 4));
		list.add(new PackItemImpl(9, 7));
		list.add(new PackItemImpl(6, 9));
		

		Dimension dimension = new Packer().pack(list);
		System.out.println("width = " + dimension.getWidth() + ", height = " + dimension.getHeight() + ", area = " + (dimension.getWidth() * dimension.getHeight()));
	}
	
}
