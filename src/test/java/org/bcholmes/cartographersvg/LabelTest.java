package org.bcholmes.cartographersvg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


import org.junit.Test;


public class LabelTest {

	
	private static final String[] FIRST_EXAMPLE = {
		"00000000 52 00 00 00 05 00 00 00 00 00 00 00 07 00 00 00 R...............",
		"00000010 00 00 13 00 00 00 00 00 D9 31 00 00 00 00 00 80 .........1......",
		"00000020 13 43 00 40 A8 43 00 00 A0 40 00 00 80 3F 00 00 .C.@.C...@...?..",
		"00000030 00 00 00 00 16 43 00 00 00 00 00 00 4F 6E 65 20 .....C......One ",
		"00000040 53 71 75 61 72 65 20 3D 20 54 65 6E 20 46 65 65 Square = Ten Fee",
		"00000050 74 00                                           t."
	};
	
	private static final String[] SECOND_EXAMPLE = {
		"00000000 51 00 00 00 05 00 00 00 00 00 00 00 07 00 00 00 Q...............",
		"00000010 00 00 01 00 00 00 00 00 C0 3A 00 00 00 01 00 00 .........:......",
		"00000020 E0 40 00 00 3C 42 00 00 80 3F 00 00 80 3F 00 00 .@..<B...?...?..",
		"00000030 00 00 00 00 C8 42 00 00 00 00 00 00 53 74 61 66 .....B......Staf",
		"00000040 66 20 26 20 44 61 67 67 65 72 20 4C 6F 64 67 65 f & Dagger Lodge",
		"00000050 00                                              ."
	};
	private static final String[] THIRD_EXAMPLE = {
		"00000000 4C 00 00 00 05 00 00 00 00 00 00 00 07 00 00 00 L...............",
		"00000010 00 00 01 00 00 00 00 00 C1 3A 00 00 00 01 00 00 .........:......",
		"00000020 E0 40 00 00 2C 42 00 00 80 3F 00 00 80 3F 00 00 .@..,B...?...?..",
		"00000030 00 00 00 00 C8 42 00 00 00 00 00 00 4D 61 65 6C .....B......Mael",
		"00000040 73 74 72 6F 6D 20 49 73 6C 65 73 00             strom Isles."
	};

	@Test
	public void shouldParseLabel() throws Exception {
		CommonHeader header = CommonHeader.fromBytes(ByteDataHelper.createBytes(FIRST_EXAMPLE), 0, 24);
		Label label = Label.from(header.rest, header);
		assertNotNull("label", label);
		assertEquals("text", "One Square = Ten Feet", label.getText());
	}

	@Test
	public void shouldParseLabel2() throws Exception {
		CommonHeader header = CommonHeader.fromBytes(ByteDataHelper.createBytes(SECOND_EXAMPLE), 0, 24);
		Label label = Label.from(header.rest, header);
		assertNotNull("label", label);
		assertEquals("text", "Staff & Dagger Lodge", label.getText());
	}
	
	@Test
	public void shouldParseLabel3() throws Exception {
		CommonHeader header = CommonHeader.fromBytes(ByteDataHelper.createBytes(THIRD_EXAMPLE), 0, 24);
		Label label = Label.from(header.rest, header);
		assertNotNull("label", label);
		assertEquals("text", "Maelstrom Isles", label.getText());
	}
}
