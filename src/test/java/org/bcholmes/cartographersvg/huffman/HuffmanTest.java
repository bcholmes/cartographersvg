package org.bcholmes.cartographersvg.huffman;

import static org.junit.Assert.assertEquals;

import org.bcholmes.cartographersvg.huffman.Huff;
import org.bcholmes.cartographersvg.huffman.Huffman;
import org.junit.Test;


public class HuffmanTest {

	private Huffman huffman = new Huffman();
	
	@Test
	public void shouldGetBit() throws Exception {
		
		Huff huff = new Huff();
		huff.in = new byte[] { 'a' };
		huff.bit = 0;
		

		// 'a' = 0x61 = 0110 0001
		
		assertEquals("bit 1", 1, this.huffman.getBit(huff));
		assertEquals("bit 2", 0, this.huffman.getBit(huff));
		assertEquals("bit 3", 0, this.huffman.getBit(huff));
		assertEquals("bit 4", 0, this.huffman.getBit(huff));
		assertEquals("bit 5", 0, this.huffman.getBit(huff));
		assertEquals("bit 6", 1, this.huffman.getBit(huff));
		assertEquals("bit 7", 1, this.huffman.getBit(huff));
		assertEquals("bit 8", 0, this.huffman.getBit(huff));
		
		assertEquals("all done", -1, this.huffman.getBit(huff));
		
	}
	@Test
	public void shouldGetByte() throws Exception {
		
		Huff huff = new Huff();
		huff.in = new byte[] { 'a' };
		huff.bit = 0;

		assertEquals("bit 1", 'a', this.huffman.GetByte(huff));
	}
	
}
