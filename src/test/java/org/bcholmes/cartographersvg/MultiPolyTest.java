package org.bcholmes.cartographersvg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.bcholmes.cartographersvg.MultiPoly.SubPath;
import org.junit.Test;


public class MultiPolyTest {

	@Test
	public void shouldJoinSimpleSubPaths() throws Exception {
		
		MultiPoly multiPoly = new MultiPoly(null, (short) 0);
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(5.0f, 205.0f), new Point(255.0f, 205.0f)), null));
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(255.0f, 5.0f), new Point(5.0f, 5.0f)), null));
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(5.0f, 5.0f), new Point(5.0f, 205.0f)), null));
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(255.0f, 205.0f), new Point(255.0f, 5.0f)), null));
		
		List<SubPath> subPaths = multiPoly.getSubPaths();
		assertNotNull("subPaths", subPaths);
		
		assertEquals("size", 1, subPaths.size());
	}
	
	@Test
	public void shouldJoinSubPathsWithReversal() throws Exception {
		
		MultiPoly multiPoly = new MultiPoly(null, (short) 0);
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(5.0f, 205.0f), new Point(255.0f, 205.0f)), null));
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(255.0f, 5.0f), new Point(5.0f, 5.0f)), null));
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(5.0f, 205.0f), new Point(5.0f, 5.0f)), null));
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(255.0f, 5.0f), new Point(255.0f, 205.0f)), null));
		
		List<SubPath> subPaths = multiPoly.getSubPaths();
		assertNotNull("subPaths", subPaths);
		
		assertEquals("size", 1, subPaths.size());
	}
	
	@Test
	public void shouldJoinSubPathsIncludingArcWithReversal() throws Exception {
		
		MultiPoly multiPoly = new MultiPoly(null, (short) 0);
		Arc arc = new Arc(null, new Point(10.0f, 10.0f), 10.0f, 0.0f, (float) (Math.PI / 2.0));
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(10.0f, 10.0f), new Point(20.0f, 10.0f)), null));
		multiPoly.getSublist().add(arc);
		multiPoly.getSublist().add(new Line(Arrays.asList(new Point(10.0f, 20.0f), new Point(10.0f, 10.0f)), null));
		
		System.out.println(arc.getFirstPoint());
		System.out.println(arc.getLastPoint());
		
		List<SubPath> subPaths = multiPoly.getSubPaths();
		assertNotNull("subPaths", subPaths);
		
		assertEquals("size", 1, subPaths.size());
	}
	
}
