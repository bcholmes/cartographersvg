package org.bcholmes.cartographersvg;

import static org.junit.Assert.*;

import org.junit.Test;


public class PointTest {

	private static final double DELTA = 0.001;

	@Test
	public void shouldSupportMultiplication() throws Exception {
		Point result = new Point(10.0f, 6.0f).multiply(0.5f);
		assertEquals("x", 5.0f, result.getX(), DELTA);
		assertEquals("y", 3.0f, result.getY(), DELTA);
	}
	
	@Test
	public void shouldSupportAddition() throws Exception {
		Point result = new Point(10.0f, 6.0f).add(new Point(1.0f, 3.0f));
		assertEquals("x", 11.0f, result.getX(), DELTA);
		assertEquals("y", 9.0f, result.getY(), DELTA);
	}
}
