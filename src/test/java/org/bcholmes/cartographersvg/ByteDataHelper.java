package org.bcholmes.cartographersvg;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.apache.commons.lang.StringUtils;

public class ByteDataHelper {

	static byte[] createBytes(String[] firstExample) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		for (int i = 0; i < firstExample.length; i++) {
			String[] parts = StringUtils.split(StringUtils.substringAfter(firstExample[i], " ").substring(0, 3*16));
			for (String string : parts) {
				output.write((byte) Integer.parseInt(string, 16));
			}
		}
		return output.toByteArray();
	}

	public static ByteBuffer createByteBuffer(String[] example1) {
		ByteBuffer buffer = ByteBuffer.wrap(createBytes(example1));
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		return buffer;
	}
}
