# CartographerSVG

So. There's this tool called ["Campaign Cartographer"](https://secure.profantasy.com/products/cc3.asp). 
It's based on a type of industrial software called "CAD" (Computer-aided design), which is commonly used for things
like blueprints and mechanical diagrams. AutoCAD is probably the most well-known CAD software, but
Campaign Cartographer is more closely associated with FastCAD.

Campaign Cartographer is probably the closest thing there is to an industry-standard RPG
mapping software product. And while, in the hands of someone fairly experienced, it's a 
pretty powerful mapping tool, I think it has a few weaknesses. First, it's Windoze-only.
Second, its UI has a bit of a learning curve. Third, the output file type (FCW) is fairly
proprietary (although it could export to PNG).

Prior to Campaign Cartographer 3, the tool was almost exclusively a vector-based diagramming
tool. It came with a number of pre-designed vector images to draw things like mountains,
caves, dungeon trap doors, and chests of gold pieces. In the early days, these symbols 
were very simple by design to ensure that they could be rendered quickly. For example, 
one of the common mountain symbols had four or five lines in it. But one might have a 
hundred of these mountain symbols on a world map. The simple design of the symbol allowed
the map to render quickly, even when viewing the entire map. As computer processing 
power increased, the symbols increased in complexity.

A few years later, Campaign Cartographer started to experience some competition from 
PNG/raster-based mapping tools such as Dundjinni, and in CC3, it adapted to support 
PNG images. 

I'm a fan of vector-based art, and liked the promise of the original format. But I never
invested the time to learn the tool, and life was even more complicated after I stopped
using Windows. So I wondered the question: how hard would it be to convert the FCW files
to SVG? I'm much more fluent with various SVG tools.

So, back in 2012, I wrote this code to experiment with conversion. I also tried my
hand at a [C++ version of the code](https://github.com/bcholmes/fcw2svg) that I could 
use as a plug-in to CC3 itself. I made a fair amount of progress, but as my Windows 
use decreased, my time spent on the project similarly decreased. In general, I'm a 
better Java programmer than I am a C++ programmer (because I use it every day). So I'd
often explore functionality in this Java codebase, and then port it to C++. 

For a long time, this code was stored in a private subversion repo, but in the interests
of openness, I figured it'd be better to be in a more easily-accessible repo. 

## Limitations

For the most part, converting FCW to SVG is fairly straight-forward, once you understand
the fairly opaque format, but there are some areas that are a bit tricker.

1. This code essentially provides no support for PNG-based diagrams.
2. CC3 "strokes" tend to be a fixed width, regardless of scale-factor, whereas SVG wants 
to specify an actual stroke width.
3. CC3 might have access to a font that isn't available (or easily findable) by SVG.
4. Layers are tricky. CC3 provides a "flag" on each object that lets it indicate if it's 
on a "furniture layer" or "wall layer". This layer flag is essentially orthogonal to object "order". 
SVG doesn't really have layers, although the SVG tool, Inkscape, uses an extension to 
SVG "groups" to emulate layers. But SVG groups (and hence Inkscape layers) have a particular
order. Occasionally, a map (particularly isometric maps) have problems with layers because of this.